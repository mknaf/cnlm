# About
A language checker based on character n-grams.

# Usage
In order to create a language model from multiple input files,
input is read from STDIN.

## Examples

    python3 train_model.py -m lm.de -o 3 < wikipedia.de

or

    cat file1 file2 file3 | python3 train_model.py -m lm.de -o 3
