#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import argparse
import codecs
import json
import os.path
import re
import sys


def train_model(lm, order):

    # regex that matches any Unicode letter, see
    # http://stackoverflow.com/a/6314634/1085954
    r = re.compile(r'[^\W\d_]+', re.U)

    # read from STDIN and calculate distributions
    ngram_counts = {}
    for line in sys.stdin.readlines():
        for i in range(len(line) - order):
            ngram = line[i:i + order]
            # don't count n-grams with non-letter characters
            if r.fullmatch(ngram) is None:
                continue
            ngram_counts[ngram] = ngram_counts.setdefault(ngram, 0) + 1

    # smooth out really rare n-grams
    ngram_counts = {k: v for (k, v) in ngram_counts.items() if v > 2}

    # convert counts to distribution
    ngram_dists = {}
    s = sum(ngram_counts.values())
    for k, v in ngram_counts.items():
        ngram_dists[k] = float(v) / s

    lm_file = codecs.open(lm, "w", "utf-8")
    json.dump(
        ngram_dists,
        lm_file,
        indent=2,
        sort_keys=True
    )
    lm_file.close()

    print(sum(ngram_dists.values()))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Train a language model on a given file."
    )
    parser.add_argument(
        "-m",
        "--model",
        type=str,
        required=True,
        help="Target language model"
    )
    parser.add_argument(
        "-o",
        "--order",
        type=int,
        required=False,
        help="Markov model order / N-gram size",
        default=2
    )

    args = parser.parse_args()

    train_model(
        args.model,
        args.order
    )
